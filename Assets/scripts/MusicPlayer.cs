﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class MusicPlayer : MonoBehaviour
{
        // Start is called before the first frame update
        void Awake()
        {
            SetUpSingleton();
        }

        private void SetUpSingleton()
        {
            var musicPlayers = FindObjectsOfType<MusicPlayer>();

        foreach (var musicPlayer in musicPlayers)
        {
            if (ReferenceEquals(musicPlayer, this))
                continue;
         
            Destroy(musicPlayer.gameObject);
        }
                        
            DontDestroyOnLoad(gameObject);
        }
    }
