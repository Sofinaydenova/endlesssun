﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectible : MonoBehaviour
{
    
    [SerializeField] AudioClip coinPickUpSFX;
    [SerializeField] int coinValue = 1;

   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
        AddCoin();
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(coinPickUpSFX, Camera.main.transform.position);
          //  gameObject.SetActive(false);
      //  Destroy(gameObject);
        }

    }

    private void AddCoin()
    {
        FindObjectOfType<GameSession>().AddToScore(coinValue);
    }


}
