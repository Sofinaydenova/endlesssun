﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSplashScreen : MonoBehaviour
{
    [SerializeField] string sceneName;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("LoadSplash", 2f);
    }

    public void LoadSplash()
    {
        SceneManager.LoadScene(sceneName);
    }
}
