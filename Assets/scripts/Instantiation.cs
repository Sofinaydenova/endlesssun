﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiation : MonoBehaviour
{
    public GameObject starPrefab;
    
    public float respawnTime = 1f;
 private Vector2 spawnerWave;
    [SerializeField] Vector3 position;

    float xMin = -50;
    float xMax = 50;
    float yMin = 30;
    float yMax = 40;

    // Start is called before the first frame update
    void Start()
    {
        spawnerWave = position;
            //Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(StarWave());
    }

    private void Spawn()
    {
        GameObject a = Instantiate(starPrefab) as GameObject;
        a.transform.position = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));
    }

    IEnumerator StarWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            Spawn();
        }
    }
}
