﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComputer : MonoBehaviour
{
    Rigidbody2D _body;
    [SerializeField] float speed = 2, jump_force = 5;
    private bool onGround = true;
    PolygonCollider2D _collider;
    Animator _animator;
    bool _isMobile = false;
    DynamicJoystick _joystick;
    public JumpButton _jumpButton;

    static HashSet<UnityEngine.RuntimePlatform> mobilePlatforms = new HashSet<UnityEngine.RuntimePlatform> {
            RuntimePlatform.IPhonePlayer,
            RuntimePlatform.Android
    };


    private void Start()
    {
        _body = GetComponent<Rigidbody2D>();
        _collider = GetComponent<PolygonCollider2D>();
        _animator = GetComponent<Animator>();
        _isMobile = PlayerComputer.mobilePlatforms.Contains(Application.platform);

        // I would suggest using LINQ
        var joysticks = FindObjectsOfType<DynamicJoystick>();
        if (joysticks.Length > 0) {
            _joystick = joysticks[0];
        }
    }


    private void Update()
    {

        /*   if (!onGround)
           {
               RaycastHit2D hit = Physics2D.Raycast( transform.position + new Vector3(0,-0.11f, 0), Vector2.down, 0.02f );
               if (hit.collider != null)
               {
                   print(hit.collider.name);
                   onGround = true;
               }
           }



          if (Input.GetKeyDown(KeyCode.Space) && onGround)
          {
              _body.AddForce(new Vector2(0, jump_force));
              onGround = false;
          }
          */


        float h = _isMobile
            ? _joystick.Horizontal
            : Input.GetAxis("Horizontal");
        
        _body.velocity = new Vector2(h * speed, _body.velocity.y);

        // Can be simplified: _animator.SetBool("Idle", h == 0);
        // Watch out when you write if and do something with Bools inside.
        _animator.SetBool("Idle", h == 0);

        if (_collider.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            Jump();
           
        }
        else { return; }

        FlipSprite();

    }
   
   void  Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) || _jumpButton.isPressed)
        {
            // _body.AddForce(new Vector2(0, jump_force));
            _animator.SetBool("Jump", true);
            Vector2 jumpVelocityToAdd = new Vector2(0f, jump_force);
               _body.velocity += jumpVelocityToAdd;
            _animator.SetBool("Jump", false);
        }
    }

    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(_body.velocity.x) > Mathf.Epsilon;
        if (playerHasHorizontalSpeed)
        {
            transform.localScale = new Vector2(Mathf.Sign(_body.velocity.x), 1f);

        }


    }
}
