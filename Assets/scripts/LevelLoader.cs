﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] string levelName;

   

   public void LoadScene()
    {
        SceneManager.LoadScene(levelName);
    }

    public void LoadStartSreen() 
        // (menu)
    {
        SceneManager.LoadScene("StartScreen");
    }
}
