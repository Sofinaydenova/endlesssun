﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSparner : MonoBehaviour
{
    [SerializeField] float minSpeed = -10f;
    [SerializeField] float maxSpeed = 19f;
    private Rigidbody2D rb;


    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(Random.Range(minSpeed,maxSpeed), Random.Range(-1, 3));
       // screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
